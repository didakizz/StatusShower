﻿using Xamarin.Forms;
using StatusShower.UI.CustomUI.Cells;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace StatusShower
{
    public static class Constants
    {
        public const string jsonStatusData = @"[
  {
      'Name': 'Donald',
      'ImageUrl': 'https://img.huffingtonpost.com/asset/59355ce52300003b00348d3f.jpeg?ops=scalefit_720_noupscale',
      'Message': 'Hey, I am the worst president ever',
  },
  {
      'Name': 'Sisi',
      'ImageUrl': 'https://static.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg',
      'Message': 'Meow, Meow, need food!', 
  },
  {
      'Name': 'Evil Recruiter',
      'ImageUrl': 'https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAUOAAAAJGU3MjE0Y2UyLTI2MmUtNDhkNi1hMzgxLWQ0N2IwMDA2Y2M4ZQ.jpg',
      'Message': 'Coming to get you!!!',
  },
  {
      'Name': 'Annoying Foodie',
      'ImageUrl': 'https://www.thecollegeview.com/wp-content/uploads/2017/09/man-eating-spaghetti-foods-make-you-hungrier.jpg',
      'Message': 'Check out my dish!',
  }
]";
    }

    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            // Get status data from json object
            var statusData = JsonConvert.DeserializeObject<List<StatusData>>(Constants.jsonStatusData);

            // Create and set up list view
            ListView listView = new ListView
            {
                RowHeight = 180
            };

            listView.ItemsSource = statusData;
            listView.ItemTemplate = new DataTemplate(typeof(StatusCell));

            Content = listView;
        }
    }
}
