﻿using System;
using Xamarin.Forms;

namespace StatusShower.Models
{
    /// <summary>
    /// ShadowEffect Interface
    /// </summary>
    public class ShadowEffect : RoutingEffect
    {
        public ShadowEffect() : base("StatusShower.LabelShadowEffect")
        {
        }

        /// <summary>
        /// Radius of Shadow Effect 
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// Color of Shadow Effect 
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Shadow Offset of Shadow Effect on the X-axis
        /// </summary>
        public float DistanceX { get; set; }

        /// <summary>
        /// Shadow Offset of Shadow Effect on the Y-axis
        /// </summary>
        public float DistanceY { get; set; }

    }
}
