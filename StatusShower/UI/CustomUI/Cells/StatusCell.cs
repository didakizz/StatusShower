﻿using StatusShower.Models;
using Xamarin.Forms;
using System;

namespace StatusShower.UI.CustomUI.Cells
{
    public class StatusCell : ViewCell
    {
        public StatusCell()
        {
            // Set up and bind cell's image
            var image = new ImageCircle
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 100,
                HeightRequest = 100,
                Aspect = Aspect.AspectFill,
            };

            image.SetBinding(Image.SourceProperty, new Binding("ImageUrl"));

            // Set up, bind and add shadow for cell's message label
            var messageLabel = new Label
            {
                HeightRequest = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            messageLabel.SetBinding(Label.TextProperty, "Message");

            // Shadow effect
            Color color = Color.Default;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    color = Color.Black;
                    break;
                case Device.Android:
                    color = Color.Black;
                    break;
            }

            messageLabel.Effects.Add(new ShadowEffect
            {
                Radius = 5,
                Color = color,
                DistanceX = 5,
                DistanceY = 5
            });

            // Set up and bind name label
            var nameLabel = new Label
            {
                HeightRequest = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            nameLabel.SetBinding(Label.TextProperty, "Name");

            // Create and set up cell's layout
            var viewLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Children = { messageLabel, image, nameLabel }
            };

            // Set cell's layout
            View = viewLayout;
        }
    }
}
