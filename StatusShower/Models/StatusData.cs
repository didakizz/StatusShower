﻿using System;

namespace StatusShower
{
    public class StatusData
    {
        public StatusData(string name, string message, string imageUrl)
        {
            this.Name = name;
            this.Message = message;
            this.ImageUrl = imageUrl;
        }

        public string Name { get; private set; }
        public string ImageUrl { get; private set; }
        public string Message { get; private set; }
    }
}
