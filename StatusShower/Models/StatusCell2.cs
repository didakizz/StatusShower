﻿using System;
using Xamarin.Forms;
namespace StatusShower.Models
                      
{
    public class StatusCell2 : ViewCell
    {
        public StatusCell2()
        {

            var image = new ImageCircle
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 180,
                HeightRequest = 180
            };

            image.SetBinding(Image.SourceProperty, new Binding("ImageUrl"));


            var messageLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Color color = Color.Default;

            switch(Device.RuntimePlatform)
            {
                case Device.iOS:
                    color = Color.Black;
                    break;

                case Device.Android:
                    color = Color.Red;
                    break;
                case Device.UWP:
                    break;
            }

            messageLabel.Effects.Add(new ShadowEffect
            {
                Radius = 5,
                Color = color,
                DistanceX = 5,
                DistanceY = 5
            });

            messageLabel.SetBinding(Label.TextProperty, "Message");

            var viewLayout = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                Children = { messageLabel, image }
            };
            View = viewLayout;
        }
    }
}
