﻿using System;
using Xamarin.Forms;
namespace StatusShower.Models
{
    public class StatusCell: ViewCell
    {
        public StatusCell()
        {
            var image = new Image
            {
                HorizontalOptions = LayoutOptions.Start
            };

            image.SetBinding(Image.SourceProperty, new Binding("ImageUrl"));
            image.WidthRequest = image.HeightRequest = 40;



            var nameLayout = CreateNameLayout();
            var viewLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children = { image, nameLayout }
            };
            View = viewLayout;
        }
        static StackLayout CreateNameLayout()
        {
            var messageLabel = new Label
            {
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            messageLabel.SetBinding(Label.TextProperty, "Message");


            var nameLayout = new StackLayout()
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Orientation = StackOrientation.Vertical,
                Children = { messageLabel }
            };
            return nameLayout;
        }
    }
}
